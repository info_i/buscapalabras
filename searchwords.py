#!/usr/bin/env pythoh3
'''
Busca una palabra en una lista de palabras
'''

import sortwords
import sys


def search_word(char, word_chars):

        left_pivot = 0
        right_pivot = len(word_chars) - 1
        found = False

        while not found and left_pivot <= right_pivot:
            test_index = (right_pivot + left_pivot) // 2
            test_number = word_chars[test_index].upper()
            if char == test_number:
                found = True
            elif char < test_number:
                right_pivot = test_index - 1
            else:
                left_pivot = test_index + 1

        if not found:
            raise Exception(f'No se encuentra la palabra.')

        return test_index


def main():

    char: str = sys.argv[1].upper()
    word: list = sys.argv[2:]

    if len(word) < 1:
        print('Error, no se ha introducido los valores necesarios.')
        sys.exit()
    else:
        try:
            ordered_list = sortwords.sort(word)
            sortwords.show(ordered_list)

            position = search_word(char, ordered_list)
            print(position)

        except Exception as e:
            print(e)
            sys.exit()


if __name__ == '__main__':
    main()
